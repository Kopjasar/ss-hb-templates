# SprintSquads TemplateEngine v1.9

## About
В данном документе будет описано как работает шаблонизатор. Изначально шаблонизатор был создан с целью заведения более **200** разнотипных платжей на 3 фронтах(**iOS/Android/Web**), однако архитектурно на основе логики шаблонизатора в можно написать большую часть однотипных экранов если провести работу по абстрогированию от сервиса, и даже маленькую игру с ответами на вопросы (мы пробовали). 

С помощью этой **коробки** можно минимизировать время которое тратит разработчик для создания сервисов платежей а в будущем возможно и любых страниц в целом, если дописать пару вещей.

## Архитектура платежей и переходов

1. Когда пользователь нажимает на таб платежей происходит запрос на 
``` 
GET: https://testapi2.homebank.kz/homebank-api/api/v2/services
```
HB API собирает данные из баз в Core и Dictionaries и собирает фронту ответ.
Приходит массив примерно таких обьектов:
```
[{
    "id": 5964,
    "name": "rudnyiVodokanal",
    "providerId": 140,
    "description": {
      "rus": "Рудненский водоканал",
      "eng": "Рудненский водоканал",
      "kaz": "Рудненский водоканал"
    },
    "categories": [
      3
    ],
    "countries": [
      85
    ],
    "regions": [
      11
    ],
    "commission": {
      "type": "static",
      "info": [
        {
          "value": 0.0
        }
      ]
    },
    "isSuspended": false,
    "isVisible": true,
    "isCustom": false,
    "hasDetails": false,
    "hasReceipt": false,
    "hasTemplateView": true
  }]
```
Для того чтобы понять нужно ли нам показывать сервис в общем списке доступных сервисов важны значения  ```isVisible, isSuspended, isCustom, hasTemplateView```, а так же есть хардкодная привязка к ```providerId```

Для того чтобы понять в какой город и категорию определить сервис мы используем массивы: ```categories, countries, regions```

Комиссия по сервису всегда приходит с бэка, в теории может быть и не статичной, однако таких сервисов пока нет.
```
"commission": {
      "type": "static",
      "info": [
        {
          "value": 0.0
        }
      ]
    }
```
2. На фронтах есть логика раутинга по сервисам(какие экраны и в каких случаях стоит открывать), и к сожалению пока что не все сервисы переведены на текущий шаблонизатор. По этому мы смотрим на значениее ```hasTemplateView``` и если оно true то мы длаеем запрос на шаблон, не забудьте заменить serviceName и вставить Bearer если тестите с инсомнии или постмана:
``` 
GET: https://testapi2.homebank.kz/homebank-api/api/v1/services/{serviceName}/template?engineVersion=1.2
```
Бэкенд проводит операции и смотрит на пачку настроек сервиса и динамично генерирует сам шаблон. На фронт приходит ответ, это уже и есть сам шаблон. Ответ выглядит так:
```
{"id":"aparuTaxi","options":{"subTitle":{"ru-RU":"Оплата услуг сервиса","kz-KZ":"Сервис қызметтеріне ақы төлеу","en-US":"Pyament for the services provided"},"canChooseBonus":false,"webStyles":[{"key":"background-image","value":"linear-gradient(103deg,#10a9ff,#005a8b)"}],"startingPageId":"initialPage"},"pages":[{"id":"initialPage","type":"normal","options":{},"containers":[{"id":"0","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0}},"blocks":[{"id":"0-0","type":"roundedContainer","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0}},"elements":[{"id":"0-0-0","parameterId":"contract","type":"inputField","options":{"mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"description":{"ru-RU":"Номер лицевого счета","kz-KZ":"Шот нөмірі","en-US":"Account number"},"webPlaceholder":{"ru-RU":"0000123","kz-KZ":"0000123","en-US":"0000123"},"formatting":"number","keyboard":"number","onResignPreCheck":{"parameterId":"contract","checkCondition":{"regex":"^[0-9]{3,}$"},"errorActions":[{"id":"rpc-0.0","type":"highlightElem","options":{"targetElem":"0-0-0","highlightParent":true,"text":{"ru-RU":"Введите корректный номер счета","kz-KZ":"Шоттың нөмірін енгізіңіз","en-US":"Please enter an account number"}}}]}}}]},{"id":"0-1","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":8,"bottom":0}},"elements":[{"id":"0-1-0","type":"label","options":{"webTextAlignment":"left","mobileTextAlignment":"left","mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0},"description":{"ru-RU":"Указан на бумажной квитанции","kz-KZ":"Қағаз түбіртегінде көрсетілген","en-US":"Indicated on paper receipt"},"icon":"template_label_info","fontSize":13}}]},{"id":"0-2","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":80}},"elements":[{"id":"0-2-0","type":"submit","parameterId":"continue","actionId":"checkContract","options":{"webTextAligment":"center","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0},"description":{"ru-RU":"Проверить","kz-KZ":"Тексеру","en-US":"Check"}}}]}]}]},{"id":"secondPage","type":"normal","options":{},"containers":[{"id":"1","type":"flat","options":{"mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0}},"blocks":[{"id":"1-0","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":0},"mobileBackgroundColor":"FFFFFF"},"elements":[{"id":"1-0-0","type":"flatList","options":{"showUnderline":true,"mobileDefaultConstraints":{"left":16,"right":16,"top":8,"bottom":0},"description":{"ru-RU":"Информация","kz-KZ":"Ақпарат","en-US":"Information"},"listData":[{"id":"1-0-0-0","label":{"ru-RU":"Номер лицевого счета","kz-KZ":"Шот нөмірі","en-US":"Account number"},"value":"{0}","dependencies":["contract"]},{"id":"1-0-0-1","label":{"ru-RU":"ФИО","kz-KZ":"Аты-жөні","en-US":"Account holder name"},"value":"{0}","dependencies":["clientName"]},{"id":"1-0-0-2","label":{"ru-RU":"Адрес","kz-KZ":"Мекен-жайы","en-US":"Account address"},"value":"{0}","dependencies":["clientAddress"]},{"id":"1-0-0-3","label":{"ru-RU":"Информация","kz-KZ":"Информация","en-US":"Information"},"value":"{0}","dependencies":["additionalInfo"]}]}},{"id":"1-0-1","type":"getInfo","parameterId":"info","actionId":"hideAllErrors","options":{"mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"description":{"ru-RU":"Баланс","kz-KZ":"Баланс","en-US":"Balance"},"buttonText":{"ru-RU":"Проверить","kz-KZ":"Тексеру","en-US":"Check"},"debtText":{"ru-RU":"Задолженность на {hour:minute}","kz-KZ":"{hour:minute}-да Қарыз","en-US":"Debt on {hour:minute}"},"positiveText":{"ru-RU":"Переплата на {hour:minute}","kz-KZ":"Переплата на {hour:minute}","en-US":"Balance on {hour:minute}"}}}]},{"id":"1-1","type":"roundedContainer","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0}},"elements":[{"id":"1-1-0","parameterId":"amount","type":"inputField","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"description":{"ru-RU":"Сумма к оплате","kz-KZ":"Төленетін сома","en-US":"Amount to pay"},"webPlaceholder":{"ru-RU":"200","kz-KZ":"200","en-US":"200"},"formatting":"priceAmount","keyboard":"decimal","onResignPreCheck":{"parameterId":"amount","checkCondition":{"range":{"min":"1","max":"1000000000","dataType":"double"}},"errorActions":[{"id":"rpc-1.0","type":"highlightElem","options":{"targetElem":"1-1-0","highlightParent":true,"text":{"ru-RU":"Введите сумму","kz-KZ":"Соманы енгізіңіз","en-US":"Please enter the amount"}}}]}}}]},{"id":"1-2","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":0}},"elements":[{"id":"1-2-0","parameterId":"commission","actionId":"commissionAlert","type":"commission","options":{"calculateFromParamId":"amount","webTextAligment":"left","mobileTextAlignment":"center","mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0},"description":{"ru-RU":"Комиссия: {0} ₸","kz-KZ":"Комиссия: {0} ₸","en-US":"commission: {0} ₸"},"highLightedText":[{"ru-RU":"{0} ₸","kz-KZ":"{0} ₸","en-US":"{0} ₸"}],"dependencies":["commission"]}}]},{"id":"1-3","type":"roundedContainer","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0}},"elements":[{"id":"1-3-0","parameterId":"card","type":"paymentSelector","options":{"mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"label":{"ru-RU":"Выберите способ оплаты","kz-KZ":"Төлем әдісін таңдаңыз","en-US":"Choose a payment method"}}}]},{"id":"1-4","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":80}},"elements":[{"id":"1-4-0","type":"submit","actionId":"showExecPage","parameterId":"overallAmount","options":{"webTextAligment":"center","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0},"description":{"ru-RU":"Оплатить {0} ₸","kz-KZ":"Төлеу үшін {0} ₸","en-US":"Pay {0} ₸"},"dependencies":["overallAmount"],"sumFromParams":["amount","commission"]}}]}]}]},{"id":"execPage","type":"execPage","options":{"actionId":"exec","displayInfo":{"card":"card","commission":"commission","amount":"amount"}},"containers":[]}],"parameters":[{"id":"contract","dataType":"string"},{"id":"continue","dataType":"double"},{"id":"clientName","dataType":"string"},{"id":"clientAddress","dataType":"string"},{"id":"additionalInfo","dataType":"string"},{"id":"amount","dataType":"double"},{"id":"info","dataType":"double"},{"id":"card","dataType":"cardData"},{"id":"overallAmount","dataType":"double"},{"id":"commission","dataType":"double"},{"id":"errorDesc","dataType":"string"}],"actions":[{"id":"hideAllErrors","type":"hideAllErrors","options":{}},{"id":"commissionAlert","type":"alert","options":{"text":{"ru-RU":"Комиссия Homebank 0 ₸ \nКомиссия агрегатора {0} ₸","kz-KZ":"Homebank комиссиясы 0 ₸ \nАгрегатордың комиссиясы {0} ₸","en-US":"Homebank Commission 0 ₸ \nAggregator commission {0} ₸"},"title":{"ru-RU":"Комиссия","kz-KZ":"Комиссия","en-US":"Commission"},"dependencies":["commission"]}},{"id":"checkContract","type":"checkRequest","options":{"parameters":[{"id":"contract","valueFromParam":"contract"},{"id":"amount","value":"10"}],"preChecks":[{"parameterId":"contract","checkCondition":{"regex":"^[0-9]{3,}$"},"errorActions":[{"id":"pc-0.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"pc-0.1","type":"highlightElem","options":{"targetElem":"0-0-0","highlightParent":true,"text":{"ru-RU":"Введите корректный номер счета","kz-KZ":"Шот нөмірін дұрыс енгізіңіз","en-US":"Please enter a correct account"}}},{"id":"pc-0.2","type":"becomeFirstResponder","options":{"targetElem":"0-0-0"}},{"id":"pc-0.3","type":"taptic","options":{"tapType":"warning"}}]}],"errorResponseActions":[{"code":"-214","responseActions":[{"id":"era-0.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"era-0.1","type":"highlightElem","options":{"targetElem":"0-0-0","highlightParent":true,"text":{"ru-RU":"Данный абонент не найден","kz-KZ":"Бұл шот табылмады","en-US":"This account was not found"}}},{"id":"era-0.2","type":"becomeFirstResponder","options":{"targetElem":"0-0-0"}},{"id":"era-0.3","type":"taptic","options":{"tapType":"warning"}}]},{"code":"other","responseActions":[{"id":"era-1.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"era-1.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"resultDescription","dataType":"string"}],"parameterId":"errorDesc"}},{"id":"era-1.2","type":"alert","options":{"text":{"ru-RU":"{0}","kz-KZ":"{0}","en-US":"{0}"},"title":{"ru-RU":"Ошибка","kz-KZ":"Қате","en-US":"Error"},"dependencies":["errorDesc"]}},{"id":"era-1.3","type":"taptic","options":{"tapType":"warning"}}]}],"successResponseActions":[{"id":"sra-0.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"sra-0.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"clientName","dataType":"string"}],"parameterId":"clientName"}},{"id":"sra-0.2","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"clientAddress","dataType":"string"}],"parameterId":"clientAddress"}},{"id":"sra-0.3","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"displayInfo","dataType":"string"}],"parameterId":"additionalInfo"}},{"id":"sra-0.4","type":"setParam","options":{"dataType":"double","valueFrom":[{"id":"balance","dataType":"double"}],"parameterId":"amount"}},{"id":"sra-0.5","type":"setParam","options":{"dataType":"double","valueFrom":[{"id":"balance","dataType":"double"}],"parameterId":"info","inverse":true}},{"id":"sra-0.6","type":"showPage","options":{"pageId":"secondPage"}}]}},{"id":"showExecPage","type":"checkRequest","options":{"parameters":[{"id":"contract","valueFromParam":"contract"},{"id":"amount","valueFromParam":"amount"}],"preChecks":[{"parameterId":"amount","checkCondition":{"range":{"min":"1","max":"1000000000000","dataType":"double"}},"errorActions":[{"id":"pc-1.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"pc-1.1","type":"highlightElem","options":{"targetElem":"1-1-0","highlightParent":true,"text":{"ru-RU":"Введите корректную сумму","kz-KZ":"Дұрыс соманы енгізіңіз","en-US":"Please enter an amount"}}},{"id":"pc-1.2","type":"becomeFirstResponder","options":{"targetElem":"1-1-0"}},{"id":"pc-1.3","type":"taptic","options":{"tapType":"warning"}}]},{"parameterId":"card","errorActions":[{"id":"pc-2.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"pc-2.1","type":"highlightElem","options":{"targetElem":"1-3-0","highlightParent":true}},{"id":"pc-2.2","type":"becomeFirstResponder","options":{"targetElem":"1-3-0"}},{"id":"pc-2.3","type":"taptic","options":{"tapType":"warning"}}]}],"errorResponseActions":[{"code":"other","responseActions":[{"id":"era-2.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"era-2.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"resultDescription","dataType":"string"}],"parameterId":"errorDesc"}},{"id":"era-2.2","type":"alert","options":{"text":{"ru-RU":"{0}","kz-KZ":"{0}","en-US":"{0}"},"title":{"ru-RU":"Ошибка","kz-KZ":"Қате","en-US":"Error"},"dependencies":["errorDesc"]}},{"id":"era-2.3","type":"taptic","options":{"tapType":"warning"}}]}],"successResponseActions":[{"id":"sra-1","type":"showPage","options":{"pageId":"execPage"}}]}},{"id":"exec","type":"execRequest","options":{"chosenCardParam":"card","parameters":[{"id":"amount","valueFromParam":"amount"}],"errorResponseActions":[{"code":"other","responseActions":[{"id":"era-3.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"era-3.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"resultDescription","dataType":"string"}],"parameterId":"errorDesc"}},{"id":"era-3.2","type":"alert","options":{"text":{"ru-RU":"{0}","kz-KZ":"{0}","en-US":"{0}"},"title":{"ru-RU":"Ошибка","kz-KZ":"Қате","en-US":"Error"},"dependencies":["errorDesc"]}},{"id":"era-3.3","type":"taptic","options":{"tapType":"warning"}}]}],"successResponseActions":[{"id":"sra-2.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"sra-2.1","type":"finishTemplate","options":{"title":{"ru-RU":"Спасибо","kz-KZ":"Рахмет","en-US":"Thanks"},"text":{"ru-RU":"Ваш платеж успешно отправлен в обработку!","kz-KZ":"Сіздің төлеміңіз сәтті өңдеуге жіберілді!","en-US":"Your payment has been successfully sent for processing!"}}}]}}]}
```
>Это jsonMinify версия ответа.
3. TemplateEngine(далее движок) принимает шаблон и обрабатывает этот json в экраны и инструкции к этим экранам. При отрисвоке экранов движок может лишь распозновать знакомые ему компоненты и выполнять действия которые ему знакомы. Снизу мы видим как движок обрабатывал данный json и отрисовывал экран и определил внутресервисную логику.

 ![Пример на iOS](https://sun9-36.userapi.com/Q0DAJBBuNzt6rH07-pbhy0JgSXT-yksu3buqaw/_wK8MVXRLSo.jpg =375x)


4. Пользователь вводит нужные ему данные и шаблонизатор лишь применяет поведенчиские признаки которые были описаны в json. Пользователь совершает оплату и выходит на главный экран. 🤺 

### Общая диаграмма взаимодействий

Обычно схема взаимодействий для оплаты сервиса на шаблонном движке выглядят примерно так, очередность действий сохранена, но может быть изменена в зависимости от действий пользователя. Ниже рассматривается дефолтный кейс, незначительные вещи могли быть упущены.

Это [Mermaid](https://mermaidjs.github.io/) Uml viewer.

---
```mermaid
sequenceDiagram

3 Клиента ->> HB Back: Запрос на весь список сервисов

Note right of 3 Клиента: Если у сервиса<br/>"hasTemplateView": true <br/>идет запрос на<br/> шаблон в /template

3 Клиента ->> HB Back: Запрос для получения шаблона
HB Back ->> HB Back: Проверка настроек сервиса и генерация шаблона
Note right of 3 Клиента: Пример:  если сервис<br/>настроен с бонусами<br/>canpaybybnous<br/> в options сервиса<br/> приходит true
HB Back ->> 3 Клиента: Шаблон для движка

alt Info step (optional)
3 Клиента -->> HB Back: info запрос для получения доп данных(баланс/фио)
HB Back -->> ПШ: Запрос для получения  данных по контракту X для сервиса Y
ПШ -->> ПШ: Если сервис оффлайновый, проверка на данные
ПШ -->> Онлайн Поставщик Y: Запрос для получения данных по контракту X
Онлайн Поставщик Y -->> ПШ: Ответ с данными
ПШ -->> ПШ: Стандартизация данных
ПШ -->> HB Back: Ответ с данными в XML
HB Back -->> HB Back: Стандартизация данных
HB Back -->> 3 Клиента: Ответ с данными
end
alt Check step
3 Клиента ->> HB Back: check запрос (POST: /payments)
HB Back ->> ПШ: Запрос check для проверки существования данных по контракту X для сервиса Y
ПШ -->> ПШ: Если сервис оффлайновый, проверка на данные
ПШ -->> Онлайн Поставщик Y: Запрос для получения данных по контракту X
Онлайн Поставщик Y ->> ПШ: Ответ с данными
ПШ ->> ПШ: Стандартизация данных
ПШ ->> HB Back: Ответ с данными и processID
HB Back ->> HB Back: Стандартизация данных
HB Back ->> 3 Клиента: Ответ с данными и paymentID
Note right of 3 Клиента: Если сервис<br/>настроен с otp<br/>приходит authorizationType<br/> в ответе на чек
end
alt Pay step
3 Клиента -->> HB Back: Запрос на подтверждение OTP
3 Клиента ->> HB Back: exec запрос (PUT: /payments)
HB Back ->> HB Back: Попытка Фриза средств
HB Back -->> 3 Клиента: ошибка если фриз не удался
HB Back ->> ПШ: Запрос PAY для проверки существования по контракту X для сервиса Y и Payment ID
ПШ -->> Онлайн Поставщик Y: Запрос на оплату по контракту X
Онлайн Поставщик Y ->> ПШ: Ответ с об успехе или об ошибке
ПШ ->> ПШ: Стандартизация данных
ПШ ->> HB Back: Ответ с успехом или ошибкой и processID
HB Back ->> HB Back: Стандартизация данных
HB Back -->> HB Back: Отмена фриза при неудаче с пш
HB Back ->> HB Back: Списание средств при успехе
HB Back ->> 3 Клиента: Ответ со статусом платежа и paymentID
3 Клиента -->> 3 Клиента: Отображение ошбики при ошибке
3 Клиента ->> 3 Клиента: Закрытие экрана платежей при успехе
end
```
---
### Статусы ошибок и версионность движка

Помимо успешного ответа **200**, может так же вернуться ответ со статусом **404** или **426** ошибки.

**404** ошибка означает что HB API не смог найти настройки для вызванного сервиса чтобы сформировать шаблон или не смог найти статичный шаблон для этого сервиса, если вы видите это на продакшне, то нужно обратиться к ребятам с бэкенда, что-то не так.

**426** ошибка означает что версия шаблонизатора устарела и бэкенд не имеет шаблона для отправленной версии шаблонизатора. На момент 18.02.2019 существуют две версии шаблонизатора **1.0 и 1.1**.

Версионность шаблонизатора существует для того чтобы описывать какие компоненты и действия может обрабатывать движок той версии, и с какая логика стоит под капотом. Представим что Шаблонизатор версии один лишь знает что такое ```inputField и button```. Мы выкатываем это на продкашн с версией движка 1.0. Фронт стучится на бэк за шаблоном для сервиса ```SSquadsPay``` с версией 1.0 - бэк смотрит хватает ли Компонентов и действий доступных для 1.0( ```inputfield/button```) для того чтобы отрисовать странцу для ```SSquadsPay```. Если по логике сервис простой, возможно этого и хватит и он вернет 200 и сам шаблон. Однако Представим что после того как все было выпущено на продкашн определенный поставщик хочет себе вместо обычногоо инпутфилда с цифрами, какой нибудь барабан.

После аппрува дизайнера, разработчики 3 фронтов создают компонент и называют его ```barabanchik```  и выкатывают релиз с версией шаблонизатора 1.1. Новый клиент стучится в бэк, однако так как работы еще не были проведены на backend, и сам шаблон не был обновлен( бэк еще не знает что такое ```barabanchik```) бэкенд возвращает прежний шаблон версии 1.0, который все еще работает даже для   1.1 потому что ```(это важно)``` все новые версии движка должны уметь обрабатывать более устаревшие версии шаблона. Далее бэкенд создает еще один инстанс шаблона версии 1.1 для сервиса ```SSquadsPay``` где использует ```barabanchik```, таким образом для старых клиентов которые еще не обновились приходит шаблон версии 1.0 с ```inputField``` где пользователь просто вводит данные, а те пользователи что обновились уже крутят ```barabanchik```. 

Теперь представим что в халык звонят поставщики сервиса и говорят: ```мы ненавидим инпутфилды потому что пользователи вводят что попало и нам приходят много запросов, оставьте только барабан```. В таком случае, происходит удаление инстанса шаблона 1.0 на бэке и теперь когда пользователи которые сидят на старых клиентах запрашивают шаблон для сервиса ```SSquadsPay``` им приходит 426 ошибка. На клиентах происходит попап с сообщением мол: "Данный сервис доступен в обновленной версии приложения хоумбанка пожалуйста обновитесь". Пользователь обновляет приложение, где версия движка 1.1 и сервис работатет корректно так как на бэкенд уже идет запрос где engineVersion=1.1
``` 
GET: https://testapi2.homebank.kz/homebank-api/api/v1/services/SSquadsPay/template?engineVersion=1.1
```

Общую логику можно проследить в этой схеме:

```mermaid
graph LR
A[Клиент] -- Запрос на шаблон сервиса с описанием версии движка фронта --> B((HP API))
B --> D(Проверка на существование сервиса)
D --> F(Проверка на Версионность)
D -- Нет какой-либо версии шаблона для этого сервиса: 404 --> A
F -- Нашелся шаблон,который подходит минимальным требованиям движка, возвращает статус 200 и сам шаблон --> A[Клиент]
F -- Нашелся шаблон, но версия движка на клиенте устарела, возвращает ошибку 426 --> A[Клиент]
```

 # Выбор типа шаблона 
Шаблоны сервисов разделены на 9 типов. То к какому конкретно сервису относятся шаблоны нужно определять с помощью check запроса от поставщика, и в зависимости от данных которые возвращает поставщик, нужно выбирать тип шаблона. Ниже таблица решений:

Обозначения:

 - account - Аккаунтовый сервис
 -  invoice - Иновойсовый сервис 
 - noBalance - с чек запроса не приходит поле "balance" 
 - balance - с чек запроса приходит поле "balance" 
 - info - с чек запроса приходит доп информация по контракту: поля "clientName" или "clientAddress" 
 - noInfo - с чек запроса не приходит доп информация по контракту: поля "clientName" или "clientAddress"
 - singleInvoice - по чековому запросу может вернуться только один инвойс
 - singleItem - по чековому запросу внутри инвойса может вернуться только один item(предмет для оплаты, пример megogo)
 - manyItems - по чековому запросу внутри инвойса может вернуться много items(коммуналка). Item в данном случае гор вода/хол вода/электричество и так далее.


```mermaid
graph LR
start --> invoice(invoice)
start --> account(account)
account(account) --> balance(balance)
account(account) --> noBalance(noBalance)
noBalance(noBalance) --> info(info)
info(info) --> accountNoBalanceNoInfo>twoPageAccountNoBalance]
noBalance(noBalance) --> noInfo(noInfo)
noInfo(noInfo) --> singlePageAccountNoBalance>singlePageAccountNoBalance]
noInfo(noInfo) --> withSelector(withSelector)
withSelector(withSelector) --> singlePageAccountNoBalanceWithPurpose>singlePageAccountNoBalanceWithPurpose]
balance(balance) --> binfo(info)
binfo(info) --> twoPageAccount>twoPageAccount]
balance(balance) --> bnoInfo(noInfo)
bnoInfo(noInfo) --> singlePageAccount>singlePageAccount]
invoice(invoice) --> manyInvoices(manyInvoices)
invoice(invoice) --> singleInvoice(singleInvoice)
singleInvoice(singleInvoice) --> singleItem(singleItem)
singleInvoice(singleInvoice) --> manyItems(manyItems)
accountTwoTab>accountTwoTab]
manyInvoices(manyInvoices) --> twoPageManyInvoiceDetailed>twoPageManyInvoiceDetailed]
singleItem(singleItem) --> twoPageInvoice>twoPageInvoice]
manyItems(manyItems) --> twoPageInvoiceDetailed>twoPageInvoiceDetailed]
```

Пример для аккаунтового - это подойдет для типа twoPageAccount:
```json
"outputParameters": {
    "balance": -2578.75,
    "balanceCurrency": "KZT",
    "clientName": "Ибраев Нуржан Темирбулатович"
  }
```
Пример для аккаунтового - это подойдет для типа singlePageAccount:
```json
"outputParameters": {
    "balance": 100,
    "balanceCurrency": "KZT"
  }
```
Пример для аккаунтового - это подойдет для типа twoPageAccountNoBalance:
```json
"outputParameters": {
    "clientName": "Олег Олегович Олегов"
  }
```

# options
Настройки самого шаблона и сервиса. Тут можно засетить стили для веба, обозначить можно ли оплачивать данный сервис бонусами и указать с какой страницы стоит начать инициализациую шаблона.

Пример options для сервиса Х:
```json
"options": {
    "subTitle": {
      "ru-RU": "Оплата услуг сервиса",
      "kz-KZ": "Сервис қызметтеріне ақы төлеу",
      "en-US": "Pyament for the services provided"
    },
    "webStyles": [
      {
        "key": "background-image",
        "value": "linear-gradient(103deg,#22C3B1,#6BB0E2)"
      }
    ],
    "startingPageId": "initialPage",
    "canPayByBonus": true
  }
```
Состоит из данных частей:

## startingPageId
Указывает templateEngine какой page показать первым.
```json
"startingPageId": "initialPage"
```
Сеттинг стартовой страницы
```swift
let currentPage = template.pages?.first(where: { (page) -> Bool in
page.id == template.options.startingPageId
})
```
Прорисовка страницы через **initialize**
```swift
private  func  initialize() {
if let p = state.currentPage {
	drawPage.onNext(p)
	}
	if  state.template.options.startingPageId  !=  state.currentPageId {
		showQuitTemplateButton.onNext(())
	}
}
```

## subTitle
>web only
>
Имеет такую же структуру как **text**.
Используется на странице как сабтайтл для описания сервиса.

```json
"subTitle": {
      "ru-RU": "Оплата услуг сервиса",
      "kz-KZ": "Сервис қызметтеріне ақы төлеу",
      "en-US": "Pyament for the services provided"
    }
```

## webStyles
Массив  css стилей которые в формате key/value которые имплементируются для страницы сервиса.

Можно добавить картинку локально и вместо градиена показать какую-то картинку, либо изменить градиент на необходимое значение.
>То что приходит в JSON
``` json
"webStyles": [
      {
        "key": "background-image",
        "value": "linear-gradient(103deg,#10a9ff,#005a8b)"
      }
    ]
```

> TypeScript Имплементация в коде
```typescript
getPaymentStyle() {
	let paymentStyle = {};
	this.responsePayment.options.webStyles.forEach(element => {
	paymentStyle[element.key] = element.value;
	});
	return paymentStyle;
}
```
>HTML
```html
<section class="top-section" [ngStyle]="getPaymentStyle()" *ngIf="dataLoaded">
```


## canPayByBonus
Описывает возможность выбора оплаты GO бонусами для сервиса. Настраивается в Core базе. Автогенерируется для всех шаблонов.

>Опциональное Bool значание. При отсутствии равно false.

```json
"canPayByBonus": true
```
Имплементация:
```swift
private var allCards: [AccountItemPickable] {
	return  state.template.options.canChooseBonus  ==  true ? bonusCards  +  paymentCards : paymentCards
}
```


# parameters
Массив переменных которые используются в шаблоне, туда можно записать значения через  ```setParam``` 
Компоненты используют параметры для сохранения определенных данных, и чтения их же, обычно используются привязки к изменению значений. Если параметр меняется а компонент к нему привязан, компонент перерисуется/изменит показываемое значение. Концептуально можно относиться к этому инстансу как к обычным переменным в коде. Можно задать изначальное значение для перменной как и через setParam так и через initalValue
>? -> Опциональная переменная
>! -> Обязательная переменная

Пример того что может прийтти в parameters
```json
"parameters": [
    {
      "id": "contract",
      "dataType": "string"
    },
    {
      "id": "continue",
      "dataType": "double"
    },
    {
      "id": "invoiceId",
      "dataType": "string"
    },
    {
      "id": "clientName",
      "dataType": "string"
    },
    {
      "id": "clientAddress",
      "dataType": "string"
    },
    {
      "id": "amount",
      "dataType": "double"
    },
    {
      "id": "invoices",
      "dataType": "array"
    },
    {
      "id": "invoiceItems",
      "dataType": "array"
    },
    {
      "id": "card",
      "dataType": "cardData"
    },
    {
      "id": "overallAmount",
      "dataType": "double"
    },
    {
      "id": "commission",
      "dataType": "double"
    },
    {
      "id": "errorDesc",
      "dataType": "string"
    }
  ]
```
```swift
struct  Parameter: Codable {
  var id: String!
  var dataType: DataType?
  var initialValue: String?
}
```

### id
>Обязательный параметр, это название переменной(параметра), по которой ее можно изменить через **setParam** или привязать компонент чтобы брал данные из этой переменной через **parameterId**
```swift
var id: String!
```

### initialValue
>Опционально. 
>
Можно задать изначальное значение для перменной. 

К примеру если вы хотите чтобы в шаблоне изначальная сумма вне зависимости от контракта была презабитыми 20 теньгами то можете передать в обьект param:
 >то что должно вернуться
```json
{
  "id": "amount",
  "dataType": "double",
  "initialValue": "20"
}
```

>Реализация в iOS
```swift
if let initialValue = parameter.initialValue {
                parameters[parameter.id] = initialValue
            }
```

### dataType
>Описание типов данных
>cardData это описание обьекта карты, пу сути dict.
```swift
switch dataType { 
	case .cardData, .dict: value = (value as? [String: AnyHashable]) ?? nil 
	case .double: value = convertToDouble(value: value)
	case .int: value = convertToInt(value: value)
	case .string: value = convertToString(value: value) 	
	case .array: value = value as? [AnyHashable] ?? nil 
}
```

# pages
Массив страниц которые могут показываться внутри шаблона для того чтобы выполнить весь возможный функционал для оплаты сервиса. Одна страница может показываться в одновременно. Навигация происходит через action **showPage**.
>Пример страниц двухстраничного аккаунтового сервиса **astanaCityLift**
```json
"pages": [
    {
      "id": "initialPage",
      "type": "normal",
      "options": {},
      "containers": [
        {
          "id": "0",
          "type": "flat",
          "options": {
            "webAlignment": "Vertical",
            "mobileDefaultConstraints": {
              "left": 0,
              "right": 0,
              "top": 0,
              "bottom": 0
            }
          },
          "blocks": [
            {
              "id": "0-0",
              "type": "roundedContainer",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 16,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "0-0-0",
                  "parameterId": "contract",
                  "type": "inputField",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Номер лицевого счета",
                      "kz-KZ": "Шот нөмірі",
                      "en-US": "Account number"
                    },
                    "webPlaceholder": {
                      "ru-RU": "0000123",
                      "kz-KZ": "0000123",
                      "en-US": "0000123"
                    },
                    "formatting": "number",
                    "keyboard": "number",
                    "onResignPreCheck": {
                      "parameterId": "contract",
                      "checkCondition": {
                        "regex": "^[0-9]{3,}$"
                      },
                      "errorActions": [
                        {
                          "id": "rpc-0.0",
                          "type": "highlightElem",
                          "options": {
                            "targetElem": "0-0-0",
                            "highlightParent": true,
                            "text": {
                              "ru-RU": "Введите корректный номер счета",
                              "kz-KZ": "Шоттың нөмірін енгізіңіз",
                              "en-US": "Please enter an account number"
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              ]
            },
            {
              "id": "0-1",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 8,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "0-1-0",
                  "type": "label",
                  "options": {
                    "webTextAlignment": "left",
                    "mobileTextAlignment": "left",
                    "mobileDefaultConstraints": {
                      "left": 0,
                      "right": 0,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Указан на бумажной квитанции, сумма к оплате мин 10 тг.",
                      "kz-KZ": "Қағаз түбіртегінде көрсетілген, сумма к оплате мин 10 тг.",
                      "en-US": "Indicated on paper receipt, сумма к оплате мин 10 тг."
                    },
                    "icon": "template_label_info",
                    "fontSize": 13
                  }
                }
              ]
            },
            {
              "id": "0-2",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 80
                }
              },
              "elements": [
                {
                  "id": "0-2-0",
                  "type": "submit",
                  "parameterId": "continue",
                  "actionId": "checkContract",
                  "options": {
                    "webTextAligment": "center",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 16,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Проверить",
                      "kz-KZ": "Тексеру",
                      "en-US": "Check"
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "id": "secondPage",
      "type": "normal",
      "options": {},
      "containers": [
        {
          "id": "1",
          "type": "flat",
          "options": {
            "mobileDefaultConstraints": {
              "left": 0,
              "right": 0,
              "top": 0,
              "bottom": 0
            }
          },
          "blocks": [
            {
              "id": "1-0",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 0
                },
                "mobileBackgroundColor": "FFFFFF"
              },
              "elements": [
                {
                  "id": "1-0-0",
                  "type": "flatList",
                  "options": {
                    "showUnderline": true,
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 8,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Информация",
                      "kz-KZ": "Ақпарат",
                      "en-US": "Information"
                    },
                    "listData": [
                      {
                        "id": "1-0-0-0",
                        "label": {
                          "ru-RU": "Номер лицевого счета",
                          "kz-KZ": "Шот нөмірі",
                          "en-US": "Account number"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "contract"
                        ]
                      },
                      {
                        "id": "1-0-0-1",
                        "label": {
                          "ru-RU": "ФИО",
                          "kz-KZ": "Аты-жөні",
                          "en-US": "Account holder name"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "clientName"
                        ]
                      },
                      {
                        "id": "1-0-0-2",
                        "label": {
                          "ru-RU": "Адрес",
                          "kz-KZ": "Мекен-жайы",
                          "en-US": "Account address"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "clientAddress"
                        ]
                      },
                      {
                        "id": "1-0-0-3",
                        "label": {
                          "ru-RU": "Информация",
                          "kz-KZ": "Информация",
                          "en-US": "Information"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "additionalInfo"
                        ]
                      }
                    ]
                  }
                },
                {
                  "id": "1-0-1",
                  "type": "getInfo",
                  "parameterId": "info",
                  "actionId": "hideAllErrors",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Баланс",
                      "kz-KZ": "Баланс",
                      "en-US": "Balance"
                    },
                    "buttonText": {
                      "ru-RU": "Проверить",
                      "kz-KZ": "Тексеру",
                      "en-US": "Check"
                    },
                    "debtText": {
                      "ru-RU": "Задолженность на {hour:minute}",
                      "kz-KZ": "{hour:minute}-да Қарыз",
                      "en-US": "Debt on {hour:minute}"
                    },
                    "positiveText": {
                      "ru-RU": "Переплата на {hour:minute}",
                      "kz-KZ": "Переплата - {hour:minute}",
                      "en-US": "Balance on {hour:minute}"
                    }
                  }
                }
              ]
            },
            {
              "id": "1-1",
              "type": "roundedContainer",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 16,
                  "bottom": 0
                },
                "hintText": {
                  "ru-RU": "Сумма оплаты должна превышать 10 ₸",
                  "kz-KZ": "Төлем мөлшері 10 ₸ асу керек.",
                  "en-US": "Payment amount must be above 10 ₸"
                }
              },
              "elements": [
                {
                  "id": "1-1-0",
                  "parameterId": "amount",
                  "type": "inputField",
                  "options": {
                    "webAlignment": "Vertical",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Сумма к оплате",
                      "kz-KZ": "Төленетін сома",
                      "en-US": "Amount to pay"
                    },
                    "webPlaceholder": {
                      "ru-RU": "200",
                      "kz-KZ": "200",
                      "en-US": "200"
                    },
                    "formatting": "priceAmount",
                    "keyboard": "decimal",
                    "onResignPreCheck": {
                      "parameterId": "amount",
                      "checkCondition": {
                        "range": {
                          "min": "9",
                          "max": "1000000000",
                          "dataType": "double"
                        }
                      },
                      "errorActions": [
                        {
                          "id": "rpc-1.0",
                          "type": "highlightElem",
                          "options": {
                            "targetElem": "1-1-0",
                            "highlightParent": true,
                            "text": {
                              "ru-RU": "Введите сумму, минимум 10 тг.",
                              "kz-KZ": "Соманы енгізіңіз, минимум 10 тг.",
                              "en-US": "Please enter the amount, min 10 tg."
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              ]
            },
            {
              "id": "1-2",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "1-2-0",
                  "parameterId": "commission",
                  "actionId": "commissionAlert",
                  "type": "commission",
                  "options": {
                    "calculateFromParamId": "amount",
                    "webTextAligment": "left",
                    "mobileTextAlignment": "center",
                    "mobileDefaultConstraints": {
                      "left": 0,
                      "right": 0,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Комиссия: {0} ₸",
                      "kz-KZ": "Комиссия: {0} ₸",
                      "en-US": "commission: {0} ₸"
                    },
                    "highLightedText": [
                      {
                        "ru-RU": "{0} ₸",
                        "kz-KZ": "{0} ₸",
                        "en-US": "{0} ₸"
                      }
                    ],
                    "dependencies": [
                      "commission"
                    ]
                  }
                }
              ]
            },
            {
              "id": "1-3",
              "type": "roundedContainer",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 16,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "1-3-0",
                  "parameterId": "card",
                  "type": "paymentSelector",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "label": {
                      "ru-RU": "Выберите способ оплаты",
                      "kz-KZ": "Төлем әдісін таңдаңыз",
                      "en-US": "Choose a payment method"
                    }
                  }
                }
              ]
            },
            {
              "id": "1-4",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 80
                }
              },
              "elements": [
                {
                  "id": "1-4-0",
                  "type": "submit",
                  "actionId": "showExecPage",
                  "parameterId": "overallAmount",
                  "options": {
                    "webTextAligment": "center",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 16,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Оплатить {0} ₸",
                      "kz-KZ": "Төлеу үшін {0} ₸",
                      "en-US": "Pay {0} ₸"
                    },
                    "dependencies": [
                      "overallAmount"
                    ],
                    "sumFromParams": [
                      "amount",
                      "commission"
                    ]
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "id": "execPage",
      "type": "execPage",
      "options": {
        "actionId": "exec",
        "displayInfo": {
          "card": "card",
          "commission": "commission",
          "amount": "amount"
        }
      },
      "containers": []
    }
  ]
```
Компоненты внутри страницы типа **normal** конструируются рекурсивно из массива blocks внутри containers.
Моделька для страницы:

```swift
struct Page: Codable {
        var id: String!
        var type: PageType!
        var options: PageOptions!
        var containers: [Container]!

        enum PageType: String, Codable {
            case normal
            case execPage
            case noInvoicePage
        }

        struct PageOptions: Codable {
            var actionId: String?
            var displayInfo: DisplayInfo?
            var mobileBackgroundColor: String?

            struct DisplayInfo: Codable {
                var commission: String!
                var card: String!
                var amount: String!
                var contract: String?
                var text: HashMap<String, String>?
                var title: HashMap<String, String>?
            }
        }
    }
```

## id
id страницы в шаблоне. Используется в качестве идентификатора при переходах. Так же используется action-ом **showPage**
```swift
var id: String!
```

## type
На данный моментт есть есть три типа страниц.
```swift
	case normal
    case execPage
    case noInvoicePage
```

```swift
private func showPage(from action: PaymentTemplateModel.Action) {
        var nextPage = state.template.getPage(for: action.options.pageId)
        if let fallbackPageId = getFallbackPageId(for: action.options.checkInvoice) {
            nextPage = state.template.getPage(for: fallbackPageId)
        }
        guard let page = nextPage else { return }
        switch page.type! {
        case .execPage:
            showExecPage(from: action, page: page)
        case .normal:
            showNextTemplatePage(from: action, page: page)
        case .noInvoicePage:
            showNoInvoicePage(from: action, page: page)
        }
        hideAllSubmitAnimations()
    }
```
### normal
Используется при отрисовки всех главных экранов, рисуется рекурсивно. Внутри находятся контейнеры, компоненты, блоки и так далее.
### execPage
Используется в тот момент когда нужно открыть хардкодно сделанную страницу оплаты с определенными параметрами из displayInfo в котором находится карта сумма и коммиссия
### noInvoicePage
Fallback страница, открывается в том случае если инвойсы к определенному контракту пришли пустые (нечего оплачивать).
Использует displayInfo в котором находится номер контракта.
```swift
if let fallbackPageId = getFallbackPageId(for: action.options.checkInvoice) {
            nextPage = state.template.getPage(for: fallbackPageId)
        }
}
```
## options

```json
{
      "id": "execPage",
      "type": "execPage",
      "options": {
        "actionId": "exec",
        "displayInfo": {
          "card": "card",
          "commission": "commission",
          "amount": "amount",
          "text": {
            "en-US": "We will notify you of new fines",
            "kz-KZ": "Біз сізге жаңа айыппұлдар туралы хабарлаймыз",
            "ru-RU": "Мы оповестим вас о новых штрафах"
          },
          "title": {
            "en-US": "Penalties not found",
            "kz-KZ": "Айыппұлдар табылмады",
            "ru-RU": "Штрафы не найдены"
          }
        }
      },
      "containers": []
    }
```
### actionID
Id action-а который должен запуститься при нажатии на кнопку подтверждения в ```execPage```
### displayInfo
Обьект с данными в котором переменные ссылаются на параметры. Создан для того чтобы данные для инициализации типов страниц ```execPage/noInvoicePage``` принимались оттуда:
```swift
private func showNoInvoicePage(from action: PaymentTemplateModel.Action,
                                   page: PaymentTemplateModel.Page) {
        guard
            let contract = PaymentBuilderUtils.convertToString(
                value: state.getParameterValue(for: page.options.displayInfo?.contract)
            )
            else { return }
        showNoInvoicePage.onNext(contract)
    }
```
```swift
private func showExecPage(from action: PaymentTemplateModel.Action,
                              page: PaymentTemplateModel.Page) {
        guard
            let response = state.currentCheckResponse,
            let serviceName = state.service?.localizedName,
            let displayInfo = page.options.displayInfo,
            let cardDict = state.getParameterValue(for: displayInfo.card) as? [String: AnyHashable],
            let cardId = cardDict["cardId"] as? String,
            let card = allCards.first(where: {
                ($0.type == .bonus && cardId == "0") || $0.identifier == cardId
            }) else { return }

        state.currentExecPageId = page.id
        let sum = PaymentBuilderUtils.convertToDouble(value: state.getParameterValue(for: displayInfo.amount)) ?? 0
        let fee = PaymentBuilderUtils.convertToDouble(value: state.getParameterValue(for: displayInfo.commission)) ?? 0
        let contract = PaymentBuilderUtils.convertToString(value: state.getParameterValue(for: displayInfo.contract))

        showExecPage.onNext((card: card,
                             serviceName: serviceName,
                             sum: sum,
                             fee: fee,
                             contract: contract,
                             response: response))
    }
```

### mobileBackgroundColor
Имеет в себе hex-овое значение цвета. Описывает background цвет всей страницы. При смене дизайна можно заменить в шаблоне.
```json
"mobileBackgroundColor": "FFFFFF"
```
Использование:
```swift
let output = viewModel.transform(input: input)
output.drawView
            .subscribe(onNext: { [weak self] (page) in
                guard let self = self else { return }
                if let color = page.options.mobileBackgroundColor {
                    self.view.backgroundColor = UIColor.colorWithHexString(color)
                } else {
                    self.view.backgroundColor = HBColors.paymentBackground.color
                }
                self.addTemplateView(view: self.createView(from: page))
            })
            .disposed(by: disposeBag)

```

## Containers
Containers и Blocks наследуют один обьект Container.
Рекурсивно спускается и рисует Контейнеры пока не встретит в своем обьекте массив Elements. 
Контейнер бывает 3 типов.
```swift
struct Container: Codable {
        var id: String!
        var type: ContainerType!
        var options: ContainerOptions!
        var blocks: [Container]?
        var elements: [Element]?

        enum ContainerType: String, Codable {
            case roundedContainer
            case flat
            case tab
        }

        struct ContainerOptions: Codable {
            var tabNames: [Localized]?
            var initialTabId: String?
            var mobileDefaultConstraints: Constraints!
            var mobileBackgroundColor: String?
            var hintText: Localized?
            var condition: BlockCondition?

            /*
            [BlockCondition] - Показывает блок если в списке параметрах объект с "paramId" 
            имеет значение равный к "value" 
            */
            struct BlockCondition(
                var paramId: String?,
                var value: String?
            )
        } 
    }
```
```swift
func createView(from
        page: PaymentTemplateModel.Page) -> UIView {

        let containerView = TStackContainerView()

        for container in page.containers ?? [] {
            let view = addConstraints(
                to: createView(from: container),
                constraints: container.options.mobileDefaultConstraints
            )
            containerView.add(view: view)
        }
        return containerView
    }
```
Можно представить что мы Открыли контейнер 1 в котром три блока.
Внутри красного квадрата в блоке 1, могут быть еще блоки и так далее. 
![enter image description here](https://i.stack.imgur.com/3eqSN.png)

### type 
Описывается в ContainerType. Есть 3 типа. 
tab -  табвьюшка у котторой внутри которой могут быть n колво элементов либо контейнеров, которые отталкиваются от mobiledefaultconstraints. Одновременно может показываться всего один таб. можно посмотреть в примере сервиса megaline.
![enter image description here](https://sun9-55.userapi.com/QjxX9Ks1YNpXIK4HH8FXKRB7r237tqZeUTVEbw/sDDF9QFaCxE.jpg)
flat - квадратная вьюшка внутри которой могут быть n колво элементов либо контейнеров, которые отталкиваются от mobiledefaultconstraints.
roundedcontainer -стилизованная вьюшка внутри которой могут быть n колво элементов либо контейнеров которые оталкиваются от mobiledefaultconstraints
## options
```swift
 struct ContainerOptions: Codable {
            var tabNames: [Localized]?
            var initialTabId: String?
            var mobileDefaultConstraints: Constraints!
            var mobileBackgroundColor: String?
            var hintText: Localized?
        }
```
### tabnames
Названия для табов при применение типа tab
![enter image description here](https://sun9-55.userapi.com/QjxX9Ks1YNpXIK4HH8FXKRB7r237tqZeUTVEbw/sDDF9QFaCxE.jpg)
### initialTabId
Указывает какой таб инициализировать как дефолтный. Ссылается на id контейнера.

### mobileDefaultConstraints
Указывает с какими констрейнтами создать текущий контейнер.

### mobileBackgroundColor
Бекграунд цвет контейнера

### hintText
Хинтовая подсказка на компонент. Отрисовывается внизу roundedContainer. При вызове ошибки на контейнер подкрашивается красным.

### Elements
Сами элементы и их настройки.
Есть 11 типов в `type` элементов которые используются в разных вариациях и очередностях для отрисовки страниц. Они используют переменные из `options` и их главным действием обозначено действие в  `actionId` а их datasource изходит из `parameterId`
```swift
struct Element: Codable {
        var id: String!
        var parameterId: String?
        var type: ElementType!
        var actionId: String?
        var options: ElementOptions!

        enum ElementType: String, Codable {
            case inputField
            case label
            case paymentSelector
            case getInfo
            case submit
            case selector
            case commission
            case flatList
            case invoiceAmount
            case invoiceList
            case invoiceDescription
            case invoiceFineList
            case invoiceAmountDescription
            case scrollableSelect
        }

        struct ElementOptions: Codable {
            enum CodingKeys: String, CodingKey {
                case showUnderline
                case isEditable
                case mobileDefaultConstraints
                case onValueChangeActions
                case formatting
                case keyboard
                case calculateFromParamId
                case mobileTextAlignment
                case descriptionText = "description"
                case buttonText
                case dependencies
                case sumFromParams
                case selectionData
                case debtText
                case positiveText
                case label
                case listData
                case setAmountParam
                case setInvoiceId
                case setInvoiceItems
                case onResignPreCheck
                case icon
                case fontSize
                case iconMatching
                case isHidden
                case detailedSubmit
                case commission
                case additionalDescription
                case amount
                case invoiceId
                case commissionText
                case amountText
                case autofill
                case toUpperCase
                case contract
                case maxInputLength
                case inputFieldMask
                /*
                [showHistoricOptions] 
                - Тип Boolean; 
                - Используется в элементе "inputField";
                - При значении "true" - "inputField" загружает список данных для выбора из subscriptions;
                */
                case showHistoricOptions
                /*
                [qrScanner] - сканнер
                - Тип barcode - считывает баркоды и обновляет параметр;
                - Тип qr - считывает текст из QR и обновляет параметр;
                - Тип dictSearch - считывает текст из QR и обновляет параметры;
                - Используется в элементе "inputField";
                */
                case qrScanner
            }

            var showUnderline: Bool?
            var isEditable: Bool?
            var mobileDefaultConstraints: Constraints!
            var onValueChangeActions: [String]?
            var formatting: Formatting?
            var keyboard: Keyboard?
            var calculateFromParamId: String?
            var mobileTextAlignment: Alignment?
            var descriptionText: Localized?
            var buttonText: Localized?
            var dependencies: [String]?
            var sumFromParams: [String]?
            var selectionData: [SelectionData]?
            var debtText: Localized?
            var positiveText: Localized?
            var label: Localized?
            var listData: [ListData]?
            var setAmountParam: String?
            var setInvoiceId: String?
            var setInvoiceItems: String?
            var onResignPreCheck: Action.ActionOptions.Precheck?
            var icon: String?
            var fontSize: Int?
            var iconMatching: IconMatching?
            var isHidden: Bool?
            var detailedSubmit: Bool?
            var commission: String?
            var additionalDescription: Localized?
            var amount: String?
            var invoiceId: String?
            var commissionText: Localized?
            var amountText: Localized?
            var autofill: autofillType?
            var inputFieldMask: String?
            var maxInputLength: Int?
            var toUpperCase: Bool?
            var contract: String?
            var showHistoricOptions: Boolean?
            var qrScanner: String?

            enum autofillType: String, Codable {
                case IIN
                case FIO
                case phoneNumber
                case firstName
                case lastName
                case middleName
                case FI
            }

            struct IconMatching: Codable {
                var defaultIcon: String?
                var stringMatching: [StringMatching]?

                struct StringMatching: Codable {
                    var icon: String?
                    var containsString: [String]?
                }
            }

            struct ListData: Codable {
                var id: String!
                var label: Localized!
                var valueFromParam: String?
                var value: String?
                var dependencies: [String]?
            }

            struct SelectionData: Codable {
                var id: String!
                var text: Localized!
            }

            enum Alignment: String, Codable {
                case left
                case right
                case center
            }

            enum Formatting: String, Codable {
                case phoneNumber
                case email
                case iin
                case priceAmount
                case space3 // ### ### ###...
                case space4 // #### #### ####...
                case number
                case dash4 // ####-####-####...
                case onay // "#### ## ##### #### ####"
                case none
            }

            enum Keyboard: String, Codable {
                case number
                case email
                case normal
                case url
                case phone
                case decimal
            }
        }
    }
```
## type
Данные типы элементов используются в шаблоне

```swift
enum ElementType: String, Codable {
            case inputField
            case label
            case paymentSelector
            case getInfo
            case submit
            case selector
            case commission
            case flatList
            case invoiceAmount
            case invoiceList
            case invoiceDescription
            case invoiceFineList
            case invoiceAmountDescription
            case scrollableSelect
            case datePicker
        }
```
### invoiceDescription
Компонент который описывает invoiceList. Показывается на странице чекаута. Можно посмотреть что оплатил клиент.
![enter image description here](https://sun3-11.userapi.com/-tO53fAofaRcICRKoLnUse3t90byBqXM8vL39w/GT9agMIzqSQ.jpg)
```json
{
                  "id": "2-0-0",
                  "parameterId": "invoiceItems",
                  "type": "invoiceDescription",
                  "options": {
                    "commission": "commission",
                    "amount": "amount",
                    "invoiceId": "invoiceId",
                    "contract": "contract",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 16,
                      "bottom": 4
                    },
                    "commissionText": {
                      "ru-RU": "Комиссия",
                      "kz-KZ": "Комиссия",
                      "en-US": "Commission"
                    },
                    "amountText": {
                      "ru-RU": "Сумма к оплате",
                      "kz-KZ": "Төленетін сома",
                      "en-US": "Amount to pay"
                    }
                  }
                }
```
### invoiceAmountDescription
Компонент который описывает invoiceAmountDescription. Показывается на странице чекаута. Можно посмотреть что оплатил клиент.
![enter image description here](https://iili.io/MSlePj.png)
```json
{
                  "id": "2-0-0",
                  "type": "invoiceAmountDescription",
                  "options": {
                    "contract": "contract",
                    "invoiceId": "invoiceId",
                    "invoices": "invoices",
                    "amount": "amount",
                    "mobileDefaultConstraints": {
                      "top": 12,
                      "left": 16,
                      "right": 16,
                      "bottom": 4
                    }
                  },
                  "parameterId": "invoiceItems"
                }
```
### invoiceList
Принимает в себя массив инвойсов и отображает их для оплаты. Изменения записывает в `setInvoiceItems`. Потом отправляется на оплату. 

![enter image description here](https://sun9-35.userapi.com/CeUaDRduQTjjSh24z9ekRVhbfdgfCSkbtvZkeA/gsjnhMkDzsg.jpg)
```json
{
                  "id": "1-1-0",
                  "type": "invoiceList",
                  "parameterId": "invoices",
                  "options": {
                    "setInvoiceId": "invoiceId",
                    "setInvoiceItems": "invoiceItems",
                    "setAmountParam": "amount",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "К оплате",
                      "kz-KZ": "Төленетін сома",
                      "en-US": "Amount to pay"
                    },
                    "iconMatching": {
                      "defaultIcon": "Other",
                      "stringMatching": [
                        {
                          "containsString": [
                            "элект",
                            "энер"
                          ],
                          "icon": "Electricity"
                        },
                        {
                          "containsString": [
                            "транспорт",
                            "машина",
                            "авто"
                          ],
                          "icon": "Transport"
                        },
                        {
                          "containsString": [
                            "телев",
                            "тв",
                            "tv"
                          ],
                          "icon": "TV"
                        },
                        {
                          "containsString": [
                            "телеф",
                            "связь",
                            "связи"
                          ],
                          "icon": "Phone"
                        },
                        {
                          "containsString": [
                            "вода",
                            "воды",
                            "водо",
                            "хол",
                            "суық",
                            "ыстық",
                            "жарсу",
                            "хвс"
                          ],
                          "icon": "Water"
                        },
                        {
                          "containsString": [
                            "газ",
                            "gas",
                            "ғас",
                            "вдго",
                            "вкго"
                          ],
                          "icon": "Gas"
                        },
                        {
                          "containsString": [
                            "тбо",
                            "мусор",
                            "вывоз",
                            "қтқ"
                          ],
                          "icon": "Trash"
                        },
                        {
                          "containsString": [
                            "megaline",
                            "idnet",
                            "интернет"
                          ],
                          "icon": "Internet"
                        },
                        {
                          "containsString": [
                            "кузет",
                            "охрана",
                            "охранник"
                          ],
                          "icon": "Shield"
                        },
                        {
                          "containsString": [
                            "налог",
                            "документ",
                            "бумаг",
                            "кск",
                            "квитанции"
                          ],
                          "icon": "Taxes"
                        },
                        {
                          "containsString": [
                            "ПККСП",
                            "здоро"
                          ],
                          "icon": "Heart"
                        }
                      ]
                    }
                  }
                }
``` 
### invoiceFineList
Принимает в себя массив инвойсов и отображает их для оплаты. Изменения записывает в `setInvoiceItems`. Потом отправляется на оплату. 

![enter image description here](https://iili.io/MSlOMb.png)
```json
{
                  "id": "1-1-0",
                  "type": "invoiceFineList",
                  "actionId": "showThirdPage",
                  "options": {
                    "setInvoiceId": "invoiceId",
                    "setAmountParam": "amount",
                    "setInvoiceItems": "invoiceItems",
                    "mobileDefaultConstraints": {
                      "top": 0,
                      "left": 16,
                      "right": 16,
                      "bottom": 0
                    }
                  },
                  "parameterId": "invoices"
                }
``` 
### invoiceAmount
Принимает в себя один `invoiceitem`. используется в оплате инвойсовых сервисах.
Копирует по функционалу `inputfield`.
![enter image description here](https://sun3-13.userapi.com/VaTy9qyoCXq7J-6Du0K3_iVfOdfnJiEgdUaGnA/5y0X6fDfidc.jpg)
```json
{
                  "id": "1-1-0",
                  "parameterId": "invoiceItems",
                  "type": "invoiceAmount",
                  "options": {
                    "setAmountParam": "amount",
                    "webAlignment": "Vertical",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Сумма к оплате",
                      "kz-KZ": "Төленетін сома",
                      "en-US": "Amount to pay"
                    },
                    "webPlaceholder": {
                      "ru-RU": "200",
                      "kz-KZ": "200",
                      "en-US": "200"
                    },
                    "formatting": "priceAmount",
                    "keyboard": "decimal",
                    "onResignPreCheck": {
                      "parameterId": "amount",
                      "checkCondition": {
                        "range": {
                          "min": "69",
                          "max": "1000000000",
                          "dataType": "double"
                        }
                      },
                      "errorActions": [
                        {
                          "id": "rpc-1.0",
                          "type": "highlightElem",
                          "options": {
                            "targetElem": "1-1-0",
                            "highlightParent": true,
                            "text": {
                              "ru-RU": "Введите сумму, мин 70 тг.",
                              "kz-KZ": "Соманы енгізіңіз, мин 70 тг.",
                              "en-US": "Please enter the amount, мин 70 тг."
                            }
                          }
                        }
                      ]
                    }
                  }
                }
```
### flatList
Информационный блок. Берет данные из listData.

![enter image description here](https://sun9-58.userapi.com/_VwIJDTIkjCCwNOm0t_deMZBWNOLC11mGbwUkg/Wu3q_GJkkgU.jpg)
```json
{
                  "id": "1-0-0",
                  "type": "flatList",
                  "options": {
                    "showUnderline": true,
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 8,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Информация",
                      "kz-KZ": "Ақпарат",
                      "en-US": "Information"
                    },
                    "listData": [
                      {
                        "id": "1-0-0-0",
                        "label": {
                          "ru-RU": "Номер лицевого счета",
                          "kz-KZ": "Шот нөмірі",
                          "en-US": "Account number"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "contract"
                        ]
                      },
                      {
                        "id": "1-0-0-1",
                        "label": {
                          "ru-RU": "ФИО",
                          "kz-KZ": "Аты-жөні",
                          "en-US": "Account holder name"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "clientName"
                        ]
                      },
                      {
                        "id": "1-0-0-2",
                        "label": {
                          "ru-RU": "Адрес",
                          "kz-KZ": "Мекен-жайы",
                          "en-US": "Account address"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "clientAddress"
                        ]
                      }
                    ]
                  }
                
```

### commission
Компонент отображения комиссии.
Записывает посчитанную коммиссию в `parameterId`. При нажатии вызывает действие `actionId`. Так как комиссия может иметь тип не `static`, подписывается на `amount` и делает пересчитывание когда `calculateFromParamId` меняется
```json
{
                  "id": "0-2-0",
                  "parameterId": "commission",
                  "actionId": "commissionAlert",
                  "type": "commission",
                  "options": {
                    "calculateFromParamId": "amount",
                    "webTextAligment": "left",
                    "mobileTextAlignment": "center",
                    "mobileDefaultConstraints": {
                      "left": 0,
                      "right": 0,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Комиссия: {0} ₸",
                      "kz-KZ": "Комиссия: {0} ₸",
                      "en-US": "commission: {0} ₸"
                    },
                    "highLightedText": [
                      {
                        "ru-RU": "{0} ₸",
                        "kz-KZ": "{0} ₸",
                        "en-US": "{0} ₸"
                      }
                    ],
                    "dependencies": [
                      "commission"
                    ]
                  }
                }
```
![enter image description here](https://sun9-10.userapi.com/eCZkSK4T80Wz2cUCDBRlZo0rRnl7DvQXIV6C-w/9o338Hi95WU.jpg)
### selector
Компонент выбора опции. 
При обновлении значения вызывает действия из `onValueChangeActions`.
Описание текста в `description`. Выборка происходит из `selectionData`.  Значение `id` записывается в `parameterId`.
```json
{
                  "id": "0-0-0",
                  "parameterId": "purpose",
                  "type": "selector",
                  "options": {
                    "showUnderline": true,
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 0
                    },
                    "onValueChangeActions": [
                      "cleanContract"
                    ],
                    "description": {
                      "ru-RU": "Выберите услугу",
                      "kz-KZ": "Қызметті таңданыз",
                      "en-US": "Choose service"
                    },
                    "webPlaceholder": {
                      "ru-RU": "Выберите услугу",
                      "kz-KZ": "Қызметті таңданыз",
                      "en-US": "Choose service"
                    },
                    "selectionData": [
                      {
                        "id": "Услуги КСК (Содержание жилья)",
                        "text": {
                          "ru-RU": "Услуги КСК (Содержание жилья)",
                          "kz-KZ": "КСК қызметтері (Тұрғын үйге техникалық қызмет көрсету)",
                          "en-US": "KSK services (Housing Maintenance)"
                        }
                      },
                      {
                        "id": "Услуги обслуживания домофона",
                        "text": {
                          "ru-RU": "Услуги обслуживания домофона",
                          "kz-KZ": "Домофон қызметтерін көрсету",
                          "en-US": "Intercom services"
                        }
                      },
                      {
                        "id": "Услуги лифта",
                        "text": {
                          "ru-RU": "Услуги лифта",
                          "kz-KZ": "Лифт қызметтері",
                          "en-US": "Elevator services"
                        }
                      }
                    ]
                  }
                }
```
### scrollableSelect
Компонент выбора опции. 
При обновлении значения вызывает действия из `onValueChangeActions`.
Выборка происходит из `selectionData`.  Значение `id` записывается в `parameterId`.

![enter image description here](https://iili.io/MSlNou.png)
```json
{
                  "id": "0-0-3",
                  "parameterId": "parkingTimeInMinute",
                  "type": "scrollableSelect",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 12,
                      "bottom": 16
                    },
                    "mobileBackgroundColor" : "FFFFFF",
                    "onValueChangeActions": [],
                    "selectionData": [
                      {
                        "id": "15",
                        "text": {
                          "ru-RU": "15 мин",
                          "kz-KZ": "15 мин",
                          "en-US": "15 min"
                        }
                      },
                      {
                        "id": "30",
                        "text": {
                          "ru-RU": "30 мин",
                          "kz-KZ": "30 мин",
                          "en-US": "30 min"
                        }
                      },
                      {
                        "id": "45",
                        "text": {
                          "ru-RU": "45 мин",
                          "kz-KZ": "45 мин",
                          "en-US": "45 min"
                        }
                      },
                      {
                        "id": "60",
                        "text": {
                          "ru-RU": "1 час",
                          "kz-KZ": "1 саг",
                          "en-US": "1 hour"
                        }
                      },
                      {
                        "id": "120",
                        "text": {
                          "ru-RU": "2 часа",
                          "kz-KZ": "2 саг",
                          "en-US": "2 hour"
                        }
                      },
                      {
                        "id": "180",
                        "text": {
                          "ru-RU": "3 часа",
                          "kz-KZ": "3 саг",
                          "en-US": "3 hour"
                        }
                      },
                      {
                        "id": "360",
                        "text": {
                          "ru-RU": "6 часов",
                          "kz-KZ": "6 сагат",
                          "en-US": "6 hours"
                        }
                      }
                    ]
                  }
                }
```
### submit
Компонент кнопки. Показывает текст из description. В description идет замена на перменные из dependencies.
```json
{
                  "id": "0-4-0",
                  "type": "submit",
                  "actionId": "checkRequest",
                  "parameterId": "overallAmount",
                  "options": {
                    "webTextAligment": "center",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 16,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Оплатить {0} ₸",
                      "kz-KZ": "Оплатить {0} ₸",
                      "en-US": "Confirm {0} ₸"
                    },
                    "dependencies": [
                      "overallAmount"
                    ],
                    "sumFromParams": [
                      "amount",
                      "commission"
                    ]
                  }
                }
```
![enter image description here](https://sun9-54.userapi.com/C4vbsIrkyNE1FbTEQfbdwnW4ydUjgNqu8TmnnQ/_i_t2_lE1Mg.jpg)
### getInfo
Компонент для получения и отображения баланса.
Имеет 2 стейта. Когда данные в parameterId пустые, и когда там есть значение.
```json
{
                  "id": "0-0-1",
                  "parameterId": "info",
                  "type": "getInfo",
                  "actionId": "getInfo",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Баланс",
                      "kz-KZ": "Баланс",
                      "en-US": "Balance"
                    },
                    "buttonText": {
                      "ru-RU": "Проверить",
                      "kz-KZ": "Тексеру",
                      "en-US": "Check"
                    },
                    "debtText": {
                      "ru-RU": "Задолженность на {hour:minute}",
                      "kz-KZ": "Задолженность на {hour:minute}-да",
                      "en-US": "Debt on {hour:minute}"
                    },
                    "positiveText": {
                      "ru-RU": "Переплата на {hour:minute}",
                      "kz-KZ": "Переплата на {hour:minute}-да",
                      "en-US": "Balance on {hour:minute}"
                    }
                  }
                }
```
![enter image description here](https://sun3-13.userapi.com/Pkejhs0bCcOPyfpQF4TjmthKFq-rabKXb5Q0kg/u66OzXH5BLQ.jpg)

### paymentSelector
Компонент выбора картты 
```
parameterId - ссылка на переменную типа card
label - текст. Выберите способ оплаты.Бонусы регулируются в настройках шаблона.
```
![enter image description here](https://sun3-13.userapi.com/-VQdfMTD2X-NYD4539ijJQsV7UyBxAxkw88otQ/mG7dH85TeZo.jpg)
### label
Просто Текст. Используется как подсказка. 
![enter image description here](https://sun9-21.userapi.com/fvMyyD7ZGC0sKmehsOzNIbIhPxBPDsqthgYzMw/Di1oc1Ci1VM.jpg)
``` 
description - сам текст
icon - название иконки в локалсотредже
webTextAlignment - алайнмент на вебе
mobileTextAlignment - алайнмент на мобилках
```
### inputfield
Текстовой инпут. Ссылается на переменную и записывает и считывает значения  из `parameterId`. 
Смотрит на переменные в опцииях:
```
formatting - форматирование текста
keyboard - клавиатура которая поднимается
autofill - автозаполнение из профайла пользователя
fontSize - размер текста
description - то что показывает как placeholder
onResignPreCheck - пречек который запускает по закрытию клавиатуры
```
![сам текстфилд](https://sun9-29.userapi.com/sk4vXzzflXZALzBIJ92Rv4yK6w1W2pSVYUxfMQ/5b8BlUjoPCw.jpg)
![Ошибка на текстфилд](https://sun9-4.userapi.com/-NhvZCsRUnuapdXIxZTaUdJH35TGtgOsVBvWsg/MOnrxx-29LU.jpg)

### datePicker
Выбор даты. Ссылается на переменную и записывает и считывает значения  из `parameterId`. 
Смотрит на переменные в опцииях:
```
dateFormat - форматирование даты
timeUnit - тип выбора даты: day, month, year
autofill - автозаполнение из профайла пользователя
description - то что показывает как placeholder
onResignPreCheck - пречек который запускает по закрытию клавиатуры
```
![photo](https://iili.io/M4whFV.png)
![photo](https://iili.io/M4w4Fp.png)

# actions
```swift
struct Action: Codable {
        var id: String!
        var type: ActionType!
        var options: ActionOptions!

        enum ActionType: String, Codable {
            case setParam
            case checkRequest
            case execRequest
            case hideAllErrors
            case hideElemError
            case setElemOption
            case showPage
            case highlightElem
            case alert
            case taptic
            case finishTemplate
            case becomeFirstResponder
            case infoRequest
            case paymentFinish
            case getInfoByAccount
            case checkAbilityToPay
            case checkAbilityToPayByPhone
            case checkAbilityToPayByAccount
        }

        struct ActionParameter: Codable {
            var id: String!
            var valueFromParam: String?
            var value: String?
        }

        struct ActionOptions: Codable {
            var dataType: DataType?
            var value: String?
            var valueFrom: [From]?
            var parameterId: String?
            var pageId: String?
            var override: Bool?
            var method: Method?
            var url: String?
            var dependencies: [String]? // parameter ids
            var errorResponseActions: [ErrorResponseAction]?
            var successResponseActions: [Action]?
            var platforms: [Platform]?
            var text: Localized?
            var tapType: TapticType?
            var parameters: [ActionParameter]?
            var preChecks: [Precheck]?
            var targetElem: String?
            var title: Localized?
            var inverse: Bool?
            var infoRequest: Bool?
            var chosenCardParam: String?
            var preRequestActions: [Action]?
            var highlightParent: Bool?
            var checkInvoice: CheckInvoice?

            struct CheckInvoice: Codable {
                var parameterId: String!
                var fallbackPageId: String!
            }

            enum TapticType: String, Codable {
                case success
                case error
                case warning
            }

            enum Platform: String, Codable {
                case iOS
                case web
                case android
            }

            struct ErrorResponseAction: Codable {
                var code: String!
                var responseActions: [Action]!
            }

            enum Method: String, Codable {
                case POST
                case GET
                case PUT
            }

            struct From: Codable {
                var id: String!
                var dataType: DataType!
            }

            struct Precheck: Codable {
                var parameterId: String!
                var checkCondition: CheckCondition?
                var errorActions: [Action]!

                struct CheckCondition: Codable {
                    var regex: String?
                    var range: Range?

                    struct Range: Codable {
                        var min: String!
                        var max: String!
                        var dataType: DataType!
                    }
                }
            }
        }
    }
```

## setParam
Используется для сеттинга параметра значениями.
`inverse` умножает на -1
`override` не сетит значение если в параметре уже есть значение которое не null.
```swift
 private func setParameter(from action: PaymentTemplateModel.Action,
                              responseDictionary: [String: AnyHashable]?) {

        guard let parameterId = action.options.parameterId else { return }
        var value: AnyHashable? = action.options.value

        if let valueFrom = action.options.valueFrom {
            value = getValue(with: valueFrom, responseDictionary: responseDictionary)
        }
        if action.options.inverse == true, let v = PaymentBuilderUtils.convertToDouble(value: value) {
            value = -1 * v
        }
        value = PaymentBuilderUtils.convertToDataType(value: value, dataType: action.options.dataType)
        
        if let serviceName = state.service?.name, serviceName == "kazakhtelecom", parameterId == "amountByAccount" || parameterId == "amountByPhone" {
            if let v = PaymentBuilderUtils.convertToDouble(value: value) {
                value = -1 * v
            }
        }
        
        if let override = action.options.override {
            if override {
                updateParameter(with: parameterId, value: value)
            } else if state.getParameterValue(for: parameterId) == nil {
                updateParameter(with: parameterId, value: value)
            }
        } else {
            updateParameter(with: parameterId, value: value)
        }
    }
```
```json
{
                "id": "era-3.1",
                "type": "setParam",
                "options": {
                  "dataType": "string",
                  "valueFrom": [
                    {
                      "id": "resultDescription",
                      "dataType": "string"
                    }
                  ],
                  "parameterId": "errorDesc"
                }
              }
```
## checkrequest/execrequest/inforequest
Запросы на HB back для проведения платежных операций.
Можно выбрать параметры которые нужно отправлять. 
- Проводится пречеки на парамеры на соответствие определенным требованиям: regex/range etc.
- Если пречеки не прошли, вызывает массив действий
- Если пречеки прошли то делает запрос
- Если запрос прошел успешно то вызывает `successResponseActions`
- Если запрос прошел неудачно то вызывает ``errorResponseActions``

```swift
private func makeCheckRequest(with action: PaymentTemplateModel.Action) {
        guard let serviceName = state.service.value?.name else { return }
        paymentService.createOperation(serviceName: serviceName, inputParams: generateParameters(from: action), inform: false)
            .asObservable()
            .subscribe(onNext: { [weak self] (response) in
                self?.state.currentCheckResponse = response
                self?.handle(
                    action: action,
                    successResponseActions: action.options.successResponseActions,
                    response: response)
            }, onError: { [weak self] (error) in
                var errorText = error.localizedDescription
                if let error = error as? HBResponseError {
                    errorText = error.message
                }
                self?.handle(
                    error: error,
                    errorResponseActions: action.options.errorResponseActions,
                    responseDictionary: ["resultDescription": errorText])
            })
            .disposed(by: rx.disposeBag)
    }
 ```
```json
{
      "id": "checkRequest",
      "type": "checkRequest",
      "options": {
        "parameters": [
          {
            "id": "contract",
            "valueFromParam": "contract"
          },
          {
            "id": "amount",
            "valueFromParam": "amount"
          }
        ],
        "preChecks": [
          {
            "parameterId": "contract",
            "checkCondition": {
              "regex": "^[0-9]{3,}$"
            },
            "errorActions": [
              {
                "id": "pc-1.0",
                "type": "setParam",
                "options": {
                  "dataType": "double",
                  "value": null,
                  "parameterId": "overallAmount"
                }
              },
              {
                "id": "pc-1.1",
                "type": "highlightElem",
                "options": {
                  "targetElem": "0-0-0",
                  "text": {
                    "ru-RU": "Введите корректный номер счета",
                    "kz-KZ": "Шоттың нөмірін дұрыс енгізіңіз",
                    "en-US": "Please enter a correct number"
                  }
                }
              },
              {
                "id": "pc-1.2",
                "type": "becomeFirstResponder",
                "options": {
                  "targetElem": "0-0-0"
                }
              },
              {
                "id": "pc-1.3",
                "type": "taptic",
                "options": {
                  "tapType": "warning"
                }
              }
            ]
          },
          {
            "parameterId": "amount",
            "checkCondition": {
              "range": {
                "min": "1",
                "max": "10000000000",
                "dataType": "double"
              }
            },
            "errorActions": [
              {
                "id": "pc-2.0",
                "type": "setParam",
                "options": {
                  "dataType": "double",
                  "value": null,
                  "parameterId": "overallAmount"
                }
              },
              {
                "id": "pc-2.1",
                "type": "highlightElem",
                "options": {
                  "targetElem": "0-1-0",
                  "highlightParent": true,
                  "text": {
                    "ru-RU": "Введите корректную сумму",
                    "kz-KZ": "Дұрыс мөлшерді енгізіңіз",
                    "en-US": "Please enter a correct amount"
                  }
                }
              },
              {
                "id": "pc-2.2",
                "type": "becomeFirstResponder",
                "options": {
                  "targetElem": "0-1-0"
                }
              },
              {
                "id": "pc-2.3",
                "type": "taptic",
                "options": {
                  "tapType": "warning"
                }
              }
            ]
          },
          {
            "parameterId": "card",
            "errorActions": [
              {
                "id": "pc-3.0",
                "type": "setParam",
                "options": {
                  "dataType": "double",
                  "value": null,
                  "parameterId": "overallAmount"
                }
              },
              {
                "id": "pc-3.1",
                "type": "highlightElem",
                "options": {
                  "targetElem": "0-3-0",
                  "highlightParent": true
                }
              },
              {
                "id": "pc-3.2",
                "type": "becomeFirstResponder",
                "options": {
                  "targetElem": "0-3-0"
                }
              },
              {
                "id": "pc-3.3",
                "type": "taptic",
                "options": {
                  "tapType": "warning"
                }
              }
            ]
          }
        ],
        "errorResponseActions": [
          {
            "code": "-214",
            "responseActions": [
              {
                "id": "era-2.0",
                "type": "setParam",
                "options": {
                  "dataType": "double",
                  "value": null,
                  "parameterId": "overallAmount"
                }
              },
              {
                "id": "era-2.1",
                "type": "highlightElem",
                "options": {
                  "text": {
                    "ru-RU": "Данный абонент не найден",
                    "kz-KZ": "Бұл шот нөмірі табылған жоқ",
                    "en-US": "This account was not found"
                  },
                  "targetElem": "0-0-0"
                }
              },
              {
                "id": "era-2.2",
                "type": "becomeFirstResponder",
                "options": {
                  "targetElem": "0-0-0"
                }
              },
              {
                "id": "era-2.3",
                "type": "taptic",
                "options": {
                  "tapType": "warning"
                }
              }
            ]
          },
          {
            "code": "other",
            "responseActions": [
              {
                "id": "era-3.0",
                "type": "setParam",
                "options": {
                  "dataType": "double",
                  "value": null,
                  "parameterId": "overallAmount"
                }
              },
              {
                "id": "era-3.1",
                "type": "setParam",
                "options": {
                  "dataType": "string",
                  "valueFrom": [
                    {
                      "id": "resultDescription",
                      "dataType": "string",
                      "jsonEncode": true // При значении "true" поле преобразовывает JSON объект из String
                    }
                  ],
                  "parameterId": "errorDesc"
                }
              },
              {
                "id": "era-3.2",
                "type": "alert",
                "options": {
                  "text": {
                    "ru-RU": "{0}",
                    "kz-KZ": "{0}",
                    "en-US": "{0}"
                  },
                  "title": {
                    "ru-RU": "Ошибка",
                    "kz-KZ": "Қателік",
                    "en-US": "Error"
                  },
                  "dependencies": [
                    "errorDesc"
                  ]
                }
              },
              {
                "id": "era-3.3",
                "type": "taptic",
                "options": {
                  "tapType": "warning"
                }
              }
            ]
          }
        ],
        "successResponseActions": [
          {
            "id": "sra-1.0",
            "type": "setParam",
            "options": {
              "dataType": "double",
              "valueFrom": [
                {
                  "id": "balance",
                  "dataType": "double"
                }
              ],
              "parameterId": "info",
              "inverse": true
            }
          },
          {
            "id": "sra-1.1",
            "type": "setParam",
            "options": {
              "dataType": "double",
              "value": null,
              "parameterId": "overallAmount"
            }
          },
          {
            "id": "sra-1.2",
            "type": "showPage",
            "options": {
              "pageId": "execPage"
            }
          }
        ]
      }
    }
```
### hideAllErrors
Обнуляет все ошибки показанные на странице
```swift
private func hideAllErrors() {
        viewModels.forEach { (viewModel) in
            viewModel.hideError()
        }
    }
```

### hideElemError
Обнуляет все ошибки показанные в элементе. ссылается на  `element.id`
Часть протокола paymentviewmodel.
```swift
protocol PaymentViewModelType {
    var parameterId: String? { get }
    var elementId: String? { get }
    var parentElementId: String? { set get }
    var value: Observable<(String?, AnyHashable?)> { get }
    var triggeredShowSelector: Observable<PaymentPickerDetails> { get }
    var triggeredShowPaymentSelector: Observable<Void> { get }
    var actioned: Observable<PaymentTemplateModel.Action> { get }
    var actionedById: Observable<String?> { get }

    func set(value: Any?)
    func update(state: PaymentViewModelState, parameterId: String?)
    func showError(errorText: String?)
    func hideError()
    func becomeFirstResponder()
    func selectionDataSelected(item: PaymentSelectionData?)
}
```
### showPage
Осуществляет переход между страницами. Станицы описаны выше в `page`
Ссылается на `page.id`
```json
{
            "id": "sra-0.4",
            "type": "showPage",
            "options": {
              "pageId": "secondPage",
              "checkInvoice": {
                "parameterId": "invoiceItems",
                "fallbackPageId": "noInvoicePage"
              }
            }
          }
```
```swift
func showPage(for id: String) {
        guard let page = template.pages.filter ({ $0.id == id }).first else { return }
        self.vc?.showNext(page)
    }
    
    func configureModel(from parameters: [PaymentTemplate.Action.ActionType.Response.Parameter], with data: Data, for pageId: String) {
        parameters.forEach {
            switch $0.type {
            case .invoices:
                if let invoices = try? JSONDecoder().decode(PaymentInvoices.self, from: data) {
                    switch invoices.type {
                    case .item(let ext):
                        guard let invoice = ext.first else { return }
                        self.parameters[$0.key] = invoice.items
                        self.parameters["invoiceId"] = invoice.accountId
                    case .detail(let ext):
                        guard let invoice = ext.first else { return }
                        self.parameters[$0.key] = invoice.invoiceDetails
                        self.parameters["invoiceId"] = invoice.accountId
                    }
                    template.removeDynamicElements(for: pageId)
                    template.addDynamicElementsFromPaymentInvoices(for: pageId, invoice: invoices, key: $0.key)
                    calculateAmount()
                }
            }
        }
    }
```
### highlightElem
Вызывает ошибочный стейт на элемент. Можно дать настройки так же подсветить контейнер в котором находится элемент.
### alert
Вызывает нативный error на весь экран с текстом
### taptic
>iOS only

Вызывает tapticEngine разных типов.
### finishTemplate
Закрывает шаблон и вызывает alert успешной транзакции.
###  becomeFirstResponder
Вызвается на textField. На мобильных устройствах вызывает фокус и клавиатуру на поле описанное в options.

# Вопросы
$$SprintSquads + \emptyset  $$

>  Если у вас будут вопросы при имплементации и дополнению шаблона вы всегда можете обратиться с вопросом к yerbol@sprintsquads.com либо оставить запрос на сайте sprintsquads.com


## UML

Каждый шаблон можно разделить на структуры, каждая из которой ответственна за собственный функционал. 


```mermaid
graph LR

1(Массив)
Переменная
4{Переменная, enum}
3((Обьект))
A>Возможное значение для enum]

```
Структура выглядит так:
```mermaid
graph LR
Шаблон((Шаблон)) -- shortname сервиса--> serviceName[id]
Шаблон((Шаблон)) -- настройки сервиса и шаблона--> 2((options))
Шаблон((Шаблон)) -- страницы для прорисовки сервиса--> 3(pages)
Шаблон((Шаблон)) -- переменные которые используются в сервисе--> 4(parameters)
Шаблон((Шаблон)) -- действия которые существуют в сервисе: чек,пей,</br>переходы между страницами, ошибки итд --> 5(actions)
4(parameters)--> id
4(parameters)--> datatype{dataType}
4(parameters)--> initialValue
datatype{dataType} --> string>string]
datatype{dataType} --> bool>bool]
datatype{dataType} --> int>int]
datatype{dataType} --> double>double]
datatype{dataType} --Обьект карты--> cardData>cardData]
datatype{dataType} --> dict>dict]
datatype{dataType} --> array>array]
2((options)) --> startingPageId
2((options)) --> subTitle
subTitle((subTitle)) --наследует обьект--> text((text))
2((options)) --массив --> webStyles(webStyles)
2((options)) --> canPayByBonus{canPayByBonus}
canPayByBonus{canPayByBonus} --> bool{bool}
bool{bool} --> false>false]
bool{bool} --> true>true]
text((text)) --> ru-RU
text((text)) --> en-US
text((text)) --> kz-KZ
webStyles(webStyles) --> key
webStyles(webStyles) --> value 
3(pages) --> pageId[id]
startingPageId --Ссылается на--> pageId[id]
3(pages) --> pageType{pageType}
3(pages) --> pageOptions((options))
3(pages) --> containers(containers)
pageType{pageType} --> normal>normal]
pageType{pageType} --> execPage>execPage]
pageType{pageType} --> noInvoicePage>noInvoicePage]
pageOptions((options)) --> mobileBackgroundColor
pageOptions((options)) --> displayInfo((displayInfo))
pageOptions((options)) --> pageactionId[actionId]
execPage>execPage] --действие при оплате--> pageactionId[actionId]
pageactionId[actionId] --> actionId[id]
5(actions) --> actionId[id]
displayInfo((displayInfo)) --> dcontract[contract]
displayInfo((displayInfo)) --> damount[amount]
displayInfo((displayInfo)) --> dcard[card]
displayInfo((displayInfo)) --> dcommission[commission]
execPage>execPage] --использует--> damount[amount]
execPage>execPage] --использует--> dcard[dcard]
execPage>execPage] --использует--> dcommission[commission]
noInvoicePage>noInvoicePage] --использует--> dcontract[сontract]
containers(containers) --> contId[id]
containers(containers) --> containertype{type}
containers(containers) --> containerOptions((options))
containers(containers) --> elements(elements)
containers(containers) --рекурсия--> blocks(blocks)
blocks(blocks) --рекурсия--> containers(containers)
containertype{type} --> tab>tab]
containertype{type} --> flat>flat]
containertype{type} --> roundedContainer>roundedContainer]
containerOptions((options)) --> tabNames(tabnames)
tabNames(tabnames) --наследует--> text((text))
containerOptions((options)) --> initialtabid[initialTabId]
containerOptions((options)) --> mobileDefaultConstraints((mobileDefaultConstraints))
mobileDefaultConstraints((mobileDefaultConstraints)) --> top
mobileDefaultConstraints((mobileDefaultConstraints)) --> bottom
mobileDefaultConstraints((mobileDefaultConstraints)) --> left
mobileDefaultConstraints((mobileDefaultConstraints)) --> right
containerOptions((options)) --> mobileBackgroundColor
containerOptions((options)) --> hintText((hintText))
hintText((hintText)) --наследует--> text((text))
elements(elements) --> elementId[id]
elements(elements) --> parameterId[parameterId]
parameterId[parameterId] --> id
elements(elements) --> elActionId[actionId]
elActionId --> actionId[id]
elements(elements) --> elType{type}
elements(elements) --> elementOptions((options))
elType{type} --> inputField>inputField]
elType{type} --> labell>label]
elType{type} --> paymentSelector>paymentSelector]
elType{type} --> getInfo>getInfo]
elType{type} --> submit>submit]
elType{type} --> selector>selector]
elType{type} --> commission>commission]
elType{type} --> flatList>flatList]
elType{type} --> invoiceAmount>invoiceAmount]
elType{type} --> invoiceList>invoiceList]
elType{type} --> invoiceDescription>invoiceDescription]
elementOptions((options)) --> showUnderline{showUnderline}
showUnderline{showUnderline} --> bool{bool}
elementOptions((options)) --> isEditable{isEditable}
isEditable{isEditable} --> bool{bool}
elementOptions((options)) --> isHidden{isHidden}
isHidden{isHidden} --> bool{bool}
elementOptions((options)) --> detailedSubmit{detailedSubmit}
detailedSubmit{detailedSubmit} --> bool{bool}
elementOptions((options)) --> toUpperCase{toUpperCase}
toUpperCase{toUpperCase} --> bool{bool}
elementOptions((options)) --> mobileDefaultConstraints((mobileDefaultConstraints))
elementOptions((options)) --> descriptionText((descriptionText))
descriptionText((descriptionText)) --наследует--> text((text))
elementOptions((options)) --> buttonText((buttonText))
buttonText((buttonText)) --наследует--> text((text))
elementOptions((options)) --> debtText((debtText))
debtText((debtText)) --наследует--> text((text))
elementOptions((options)) --> positiveText((positiveText))
positiveText((positiveText)) --наследует--> text((text))
elementOptions((options)) --> label((label))
label((label)) --наследует--> text((text))
elementOptions((options)) --> additionalDescription((additionalDescription))
additionalDescription((additionalDescription)) --наследует--> text((text))
elementOptions((options)) --> commissionText((commissionText))
commissionText((commissionText)) --наследует--> text((text))
elementOptions((options)) --> amountText((amountText))
amountText((amountText)) --наследует--> text((text))
elementOptions((options)) --> ovca(onValueChangeActions)
ovca(onValueChangeActions) --> id
elementOptions((options)) --> dependencies(dependencies)
dependencies(dependencies) --> id
elementOptions((options)) --> sumFromParams(sumFromParams)
sumFromParams(sumFromParams) --> id
elementOptions((options)) --> calculateFromParamId[calculateFromParamId]
calculateFromParamId[calculateFromParamId] --> id
elementOptions((options)) --> setAmountParam[setAmountParam]
setAmountParam[setAmountParam] --> id
elementOptions((options)) --> setInvoiceId[setInvoiceId]
setInvoiceId[setInvoiceId] --> id
elementOptions((options)) --> setInvoiceId[setInvoiceId]
setInvoiceId[setInvoiceId] --> id
elementOptions((options)) --> setInvoiceItems[setInvoiceItems]
setInvoiceItems[setInvoiceItems] --> id
elementOptions((options)) --> commission[commission]
commission[commission] --> id
elementOptions((options)) --> amount[amount]
amount[amount] --> id
elementOptions((options)) --> invoiceId[invoiceId]
invoiceId[invoiceId] --> id
elementOptions((options)) --> inputFieldMask[inputFieldMask]
elementOptions((options)) --> contract[contract]
contract[contract] --> id
elementOptions((options)) --> maxInputLength[maxInputLength]
elementOptions((options)) --> fontSize[fontSize]
elementOptions((options)) --> icon[icon]
elementOptions((options)) --> formatting{formatting}
elementOptions((options)) --> keyboard{keyboard}
elementOptions((options)) --> mobileTextAlignment{mobileTextAlignment}
elementOptions((options)) --> listdata((listData))
elementOptions((options)) --> onResignPreCheck(onResignPreCheck)
onResignPreCheck(onResignPreCheck) --> precheck(preCheck)
elementOptions((options)) --> iconMatching((iconMatching))
elementOptions((options)) --> autofill{autofill}
5(actions) --> actionType{type}
5(actions) --> actionOptions((options))
actionType{type} --> setParam>setParam]
actionType{type} --> checkRequest>checkRequest]
actionType{type} --> execRequest>execRequest]
actionType{type} --> hideAllErrors>hideAllErrors]
actionType{type} --> hideElemError>hideElemError]
actionType{type} --> setElemOption>setElemOption]
actionType{type} --> highlightElem>highlightElem]
actionType{type} --> alert>alert]
actionType{type} --> taptic>taptic]
actionType{type} --> finishTemplate>finishTemplate]
actionType{type} --> becomeFirstResponder>becomeFirstResponder]
actionType{type} --> infoRequest>infoRequest]
actionType{type} --> paymentFinish>paymentFinish]
actionType{type} --> getInfoByAccount>getInfoByAccount]
actionOptions((options)) --> dataType{dataType}
actionOptions((options)) --> value
actionOptions((options)) --> valueFrom(valueFrom)
valueFrom(valueFrom) --> id
actionOptions((options)) --> actionnparId[parameterId]
actionnparId[parameterId] --> id
actionOptions((options)) --> apageId[pageId]
apageId[pageId] --> actionId[id]
actionOptions((options)) --> override{override}
override{override} --> bool{bool}    
actionOptions((options)) --> dependencies(dependencies)
dependencies(dependencies) --> id
actionOptions((options)) --> errorResponseActions(errorResponseActions)
errorResponseActions(errorResponseActions) --наследует--> 5(actions)
actionOptions((options)) --> successResponseActions(successResponseActions)
successResponseActions(successResponseActions) --наследует--> 5(actions)
actionOptions((options)) --> text(text)
actionOptions((options)) --> tapType{tapType}
```

