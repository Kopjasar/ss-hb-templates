--> PUT https://api2.homebank.kz/hbapi/api/v1/payments 

{"inputParameters":{"amount":236856.30,"cardId":151288582,"cardMask":"•••• 4833","contract":"6736-1734-9367","currency":"KZT","invoiceDetails":[{"class":"","calculation":"","id":"1","name":"6736-1734-9367","params":[{"editable":true,"paramCode":"Amount","paramId":"FixSum","paramName":"Сумма","paramValue":"236856.30","visable":true},{"editable":false,"paramCode":"","paramId":"booking_id","paramName":"Номер брони","paramValue":"7RJQGKN1","visable":true},{"editable":false,"paramCode":"","paramId":"description","paramName":"Описание заказа","paramValue":"Авиабилеты Алматы - Стамбул - Алматы, бронь №7RJQGKN1, для 1 взрослого","visable":true},{"editable":false,"paramCode":"","paramId":"payer_name","paramName":"Покупатель","paramValue":"Мухамеджанова Карлыгаш","visable":true},{"editable":false,"paramCode":"","paramId":"Сurrency","paramName":"Валюта","paramValue":"KZT","visable":true}]}],"invoiceId":"1","isSaveCard":false},"paymentId":1180202403,"serviceName":"ticketsKz","verificationId":"d6bcc5a0-4ca4-41e5-8db8-cd3cc0e4c80d"}


 <-- 400 https://api2.homebank.kz/hbapi/api/v1/payments (1061ms)
   {
      "resultCode": -269,
      "resultDescription": "Недостаточно средств на карте. Пополните карту или укажите меньшую сумму  ",
      "data": null
    }