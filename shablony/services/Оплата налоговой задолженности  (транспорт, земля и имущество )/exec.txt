--> PUT https://api2.homebank.kz/hbapi/api/v1/payments

 {"inputParameters":{"amount":18069.0,"cardId":151288582,"cardMask":"•••• 4833","contract":"870910450941","currency":"KZT","invoiceDetails":[{"id":"451204-104402-18069.00","name":"Hалог на транспортные средства с физических лиц 104402","params":[{"editable":false,"paramCode":"Amount","paramId":"FixSum","paramName":"Долг","paramValue":"18069.00","visable":true},{"editable":false,"paramCode":"","paramId":"taxRid","paramName":"Код налогового органа","paramValue":"451204","paramValueDescription":"Жылы-Булакский сельский округ Щербактинского района","visable":true},{"editable":false,"paramCode":"","paramId":"budgetCode","paramName":"Код бюджетной классификации","paramValue":"104402","paramValueDescription":"Hалог на транспортные средства с физических лиц","visable":true},{"editable":false,"paramCode":"","paramId":"purposeCode","paramName":"КНП","paramValue":"911","paramValueDescription":"Начисленные (исчисленные) и иные обязательства в бюджет","visable":true},{"editable":true,"paramCode":"","paramId":"PVIN","paramName":"VIN код","paramValue":"123466","visable":true}]}],"invoiceId":"204512","isSaveCard":false},"paymentId":1181099111,"serviceName":"taxDebt"}



     <-- 400 https://api2.homebank.kz/hbapi/api/v1/payments (1183ms)
 {
      "resultCode": -269,
      "resultDescription": "Недостаточно средств на карте. Пополните карту или укажите меньшую сумму  ",
      "data": null
    }